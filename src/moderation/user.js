module.exports = {
	'avatar': {
		details: 'Displays a user\'s avatar. If no user was mentioned, your own is displayed',
		run: data => {
			const user = data.msg.mentions.users.first();

			data.msg.channel.send(
				user ? user.avatarURL : data.msg.author.avatarURL
			);
		}
	}
};

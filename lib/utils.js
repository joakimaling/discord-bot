module.exports = {
	defaultChannel: async guild => {
		try {
			// override with a custom channel
			return guild.channels.find(
				'name',
				require('../config.json').defaultChannel
			);
		}
		catch(e) {
			// get the "original" channel
			if (guild.channels.has(guild.id)) {
				return guild.channels.get(guild.id);
			}

			// chack for a "general" channel
			if (guild.channels.exists('name', 'general')) {
				return guild.channels.find('name', 'general');
			}

			// get the first channel where the bot can speak
			return guild.channels.filter(
				c => c.type == 'text' &&
					c.permissionsFor(guild.client.user).has('SEND_MESSAGES')
			).sort(
				(a, b) => a.position - b.position ||
					Long.fromString(a.id).sub(Long.fromString(b.id)).toNumber()
			).first();
		}
	},
	meow: (args, msg, type, needMessage) => {
		const yeah = require('./types.json')[type];
		const user = msg.mentions.users.first();
		let message = '';

		if (!user) {
			msg.reply('You must mention a user.');
			return;
		}

		if (user.bot) {
			msg.reply(`You can't ${type} me.`);
			return;
		}

		if (user.equals(msg.author)) {
			msg.reply(`You can't ${type} yourself.`);
			return;
		}

		if (needMessage) {
			args.shift();

			message = args.join(' ');

			if (!message) {
				msg.reply('You must write a message.');
				return;
			}
		}

		console.log(`${msg.author.username} ${yeah} ${user.username}.`);

		user.send(`${msg.author.username} ${yeah} you.${message}`);
	}
};

/**
 * © 2018 Tygrbot
 *
 * Author: Joakim Åling <joakim.aling@gmail.com>
 *
 * Licensed under the MIT License
 * Read "LICENSE" for the full MIT License
 */
const {name, version} = require('./package.json');
const {format} = require('util');

const Discord = require('discord.js');

const client = new Discord.Client();

// default configuration
const config = {
	authorised: [
		'administrator',
		'moderator'
	],
	prefix: '!'
};

// require the token and exit if it's missing
try {
	client.login(require('./auth.json').token);
}
catch(e) {
	console.error('Missing auth.json file. Create it using this format:\n{\n  "token": "YOUR-TOKEN"\n}');
	process.exit();
}

// merge with the custom configuration, if the file exists
try {
	Object.assign(config, require('./config.json'));
}
catch(e) {
}

// global variables
let allCommands;
let helpDialogue;

//----MPD----------------------------------------------------------------------
const mpd = require('mpd');
let mpdString;

const mpc = mpd.connect({
	host: 'nanami',
	port: 6600
});

mpc.on('ready', () => {
	console.log('Connected to MPD at %s:%d', mpc.socket._host, 6600);
//	fetchSong();
});

mpc.on('system', () => {
	fetchSong();
});

function fetchSong() {
	mpc.sendCommands(['currentsong', 'status'], (error, message) => {
		const data = mpd.parseKeyValueMessage(message);

		if (data.state == 'play') {
			mpdString = `Master is listening to ${data.Title} by ${data.Artist} now.`;
			console.log(`[MPD] Playing ${data.Title} by ${data.Artist}`);

			client.user.setActivity(
				`${data.Title} by ${data.Artist}`,
				{type: 'LISTENING'}
			);
		}
		else {
			mpdString = 'Master is not listening to music at the moment.';
			console.log(
				'[MPD] Playback was %s.',
				data.state == 'stop' ? 'stopped' : 'paused'
			);
		}
	});
}
//----MPD----------------------------------------------------------------------

// when the bot is online and ready to serve
client.on('ready', async () => {
	// read the files containing available commands into the object
	allCommands = Object.assign(
		require('./lib/requireDir.js')('./src'),
		{
			'music': {
				details: 'See what music Master is listening to at the moment. Master\'s taste is exquisite!',
				run: data => {
					data.msg.channel.send(mpdString);
				}
			}
		}
	);

	// construct the help dialogue
	helpDialogue = new Discord.RichEmbed()
		.setTitle(`What is ${client.user.username} able to do?`)
		.setThumbnail(client.user.avatarURL);

	// add a field with details for each command
	for (let name in allCommands) {
		helpDialogue.addField(name, allCommands[name].details, true);
	}

	console.log(
		'[%sConnected%s] Logged in as %s (%d)\nPresent in %d guilds with %d channels and %d users\nRunning %s v%s using: \n  Discord.js v%s\n  Node.js %s\n',
		'\x1b[32m',
		'\x1b[0m',
		client.user.username,
		client.user.id,
		client.guilds.size,
		client.channels.size,
		client.users.size,
		name,
		version,
		Discord.version,
		process.version
	);
});

// when the bot is disconnected
client.on('disconnect', event => {
	console.log(
		'[%sDisconnected%s] %s',
		'\x1b[31m',
		'\x1b[0m',
		event.reason
	);
});

// outputs debug messages
if (process.argv.includes('--debug')) {
	client.on('debug', data => {
		console.info(
			'[%sDebug%s] %s',
			'\x1b[36m',
			'\x1b[0m',
			data
		);
	});
}

// when an error occurs
client.on('error', data => {
	console.error(
		'[%sError%s] %s',
		'\x1b[31m',
		'\x1b[0m',
		JSON.stringify(data, null, 2)
	);
});

// outputs warning messages
client.on('warn', data => {
	console.warn(
		'[%sWarning%s] %s',
		'\x1b[33m',
		'\x1b[0m',
		data
	);
});

// when the bot joins a guild
client.on('guildCreate', guild => {
	console.log(
		'%s has joined %s (%d).\nIt has %d members',
		client.user.username,
		guild.name,
		guild.id,
		guild.memberCount
	);
});

// when the bot leaves a guild
client.on('guildDelete', guild => {
	console.log(
		'%s has left %s (%d).',
		client.user.username,
		guild.name,
		guild.id
	);
});

// when a new member joins the guild
client.on('guildMemberAdd', member => {
	const channel = require('./lib/utils.js').defaultChannel(member.guild);

	channel.send(format(
		'Hello, %s! Welcome to %s. I\'m %s, the housekeeper of this guild. Be sure to check in here frequently. Have fun!',
		member,
		member.guild.name,
		client.user.username
	));
});

// when a message was posted on the server
client.on('message', async msg => {
	if (msg.author.bot) return;

//	msg.content

	// check if the message starts with the prefix
	if (msg.content.trimLeft().startsWith(config.prefix)) {
		const args = msg.content.trim().slice(config.prefix.length).split(/\s+/);
		const cmd = args.shift().toLowerCase();

		// check if the command isn't resticted or if it is and the user has the
		// correct role(s), then run the callback function of the command. if
		// the requested command doesn't exist - display the help dialogue
		if (cmd in allCommands) {
			if (!allCommands[cmd].restricted || msg.member.roles.some(role => config.authorised.includes(role.name))) {
				console.log(`${msg.author.username} is running ${config.prefix + cmd}.`);
				allCommands[cmd].run({args, client, config, msg, version});
			}
			else {
				msg.reply('You don\'t have the required role to do that.');
			}
		}
		else if (cmd == 'help') {
			console.log(`${msg.author.username} is running ${config.prefix + cmd}.`);
			msg.channel.send(args[0] in allCommands
				? allCommands[args[0]].details
				: helpDialogue
			);
		}
	}
});

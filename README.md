# discord-bot

> Simple bot for Discord

[![Licence][licence-badge]][licence-url]

My attempt in creating a fun, yet useful, [Discord](https://discordapp.com/) bot
using the popular [discord.js](https://discord.js.org/#/) library hoping it will
entertain and simplify repetitive tasks for users.

## Installation

:bulb: Requires [Node.js][node-url].

Run these commands replacing `YOUR-TOKEN` with your own:

```sh
git clone https://gitlab.com/joakimaling/discord-bot.git
cd discord-bot
printf '{\n  "token": "YOUR-TOKEN"\n}' > auth.json
npm install
node index
```

## Usage

The file `./config.json` is where the configuration of the bot is located. It's
not present in the beginning, but is *automatically generated* if any of the
default values are changed. The default values are as follows:

```json
{
  "authorised": [
    "administrator",
    "moderator"
  ],
  "prefix": "!"
}
```

Configuration | Details
------------- | ----------------------------------------------------------------
authorised    | An array of roles which are allowed to run restricted commands
prefix        | A string which allows the bot to separate commands from messages

### Token

In the file `./auth.json` the bot user token is located. It is not present in
the beginning and *must manually be generated* in order for the bot to work. The
format of the file is as follows:

```json
{
  "token": "YOUR-TOKEN"
}
```

### Commands - moderation

Bolded commands may only be run by users with administrator/moderator privileges
in order for the guild not to fall into anarchy.

Command      | Parameters              | Details
------------ | ----------------------- | ---------------------------------------
**addrole**  | `@user [role ...]`      | Adds role(s) to a user. If no role is given roles set by the `autorole` command are used
**alive**    |                         | Sends a message to a custom, or the default, channel about updates
**autorole** | `role [role ...]`       | Sets the role(s) given to users when they join the guild
avatar       | `[@user]`               | Displays a user's avatar. If no user was mentioned, your own is displayed
**ban**      | `@user [reason]`        | Bans a user from the guild and DMs the reason. If no reason is given then a default one set by the command `reason` is used
**clone**    | `@channel [name]`       | Clones a channel
**delrole**  | `role [role ...]`       | Deletes role(s). Users with these roles will have them removed
discrim      | `discrim`               | Finds a user with given discrim number
guildinfo    | `guild`                 | Displays information about a guild
help         | `[command]`             | Displays information about a command, or all, if no command was given
info         |                         | Displays information about the bot
invite       |                         |
**kick**     | `@user [reason]`        | Kicks a user from the guild and DMs the reason. If no reason is given then a default one set by the command `reason` is used
ping         |                         | Checks the roundtrip latency by sending a ping
**prefix**   | `prefix`                | Changes the command prefix and saves it to `./config.json`
**putrole**  | `@user role [role ...]` | Adds a user to a role
**reason**   | `reason`                | Sets the default reason which is used when running commands requiring a reason and where it's omitted
**remrole**  | `@user role [role ...]` | Removes a user from a role
roleinfo     | `role`                  | Displays information about a given role
**say**      | `[tts] message`         | Causes the bot to say what you want. If `tts` is provided, it'll use the text-to-speech feature
userinfo     | `[@user]`               | Displays user info, of a mentioned user or self
**warn**     | `user [reason]`         | Sends a DM to a user with a warning

### Commands - entertainment

These commands can be run by everyone.

Command    | Parameters         | Details
---------- | ------------------ | ----------------------------------------------
8ball      | `question`         | Ask a question and ancient wisdom will be bestowed upon you, feble human
accuse     | `@user accusation` | Sends an accusation from you to the mentioned user via DM
anime      |                    | Gives you a random anime recommendation from myanimelist.net
compliment | `@user complement` | Sends a compliment from you to the mentioned user via DM
contact    | `@user message`    | Sends a message from you to the mentioned user via DM
divorce    | `@user`            |
flip       |                    | Make ground-breaking decisions with the flip of a coin
hug        | `@user`            | Sends a hug from you to the mentioned user via DM
insult     | `@user insult`     | Sends an insult from you to the mentioned user via DM
jankenpon  |                    | Try your luck in this Japanese-style rock-paper-scissors
kinky      | `@user`            |
kiss       | `@user`            | Sends a kiss from you to the mentioned user via DM
love       | `@user`            |
marry      | `@user`            |
music      | `[@user]`          | See what music Master is listening to at the moment. Master's taste is exquisite!
number     | `numbers`          | Replaces a number sequence with clocks
poll       | `text o1 o2 [o3 ...]` |
purge      | `@user`            |
roll       |                    | Roll a die of chance which may turn the universe inside-out

## Licence

Released under MIT. See [LICENSE][licence-url] for more.

Coded with :heart: by [joakimaling][user-url].

[git-url]: https://git-scm.com/downloads/
[node-url]: https://nodejs.org/en/download/
[licence-badge]: https://badgen.net/gitlab/license/joakimaling/discord-bot
[licence-url]: LICENSE
[user-url]: https://gitlab.com/joakimaling

'use strict';

const fs = require('fs');
const path = require('path');

const walkDir = d =>
	fs.readdirSync(d).reduce((files, f) =>
		fs.statSync(path.join(d, f)).isDirectory()
			? files.concat(walkDir(path.join(d, f)))
			: files.concat(path.join(d, f)),
		[]);

module.exports = d => {
	d = path.resolve(path.dirname(module.parent.filename), d);
	return walkDir(d).reduce((a, b) => Object.assign(a, require(b)), {});
};

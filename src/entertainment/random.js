const randomInt = require('random-int');

module.exports = {
	'8ball': {
		details: 'Ask a question and ancient wisdom will be bestowed upon you, feble human',
		run: data => {
			if (data.args.join(' ').endsWith('?')) {
				data.msg.reply(['No.', 'Maybe.', 'Probably', 'Yes.'][randomInt(4)]);
			}
		}
	},
	'flip': {
		details: 'Make ground-breaking decisions with the flip of a coin',
		run: data => {
			data.msg.reply(['Heads.', 'Tails.'][randomInt(2)]);
		}
	},
	'roll': {
		details: 'Roll a die of chance which may turn the universe inside-out',
		run: data => {
			const number = ['one', 'two', 'three', 'four', 'five', 'six'][randomInt(6)];
			data.msg.reply(`You got a :${number}:`);
		}
	}
}

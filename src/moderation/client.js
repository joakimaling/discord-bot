const {format} = require('util');
const {defaultChannel} = require('../../lib/utils.js');

module.exports = {
	'alive': {
		details: 'Sends a message to a custom, or the default, channel about updates',
		run: data => {
			defaultChannel(data.msg.guild).then(channel => channel.send(format(
				'@everyone version %s of %s is now online.',
				data.version,
				data.client.user.username
			)));
		}
	},
	'ping': {
		details: 'Checks the roundtrip latency by sending a ping',
		run: async data => {
			const m = await data.msg.reply('Ping?');

			m.edit(format(
				'Pong! Message latency is %dms, API letency is %dms',
				m.createdTimestamp - data.msg.createdTimestamp,
				Math.round(data.client.ping)
			));
		}
	},
	//  'info': {},
	'prefix': {
		details: 'Changes the command prefix and saves it to `./config.json`',
		restricted: true,
		run: data => {
			if (data.args[0] && data.config.prefix != data.args[0]) {
				data.config.prefix = data.args[0];

				require('fs').writeFile(
					'./config.json',
					JSON.stringify(data.config, null, 2),
					error => {
						if (error) {
							return console.error(
								'Failed writing prefix to ./config.json'
							);
						}

						data.msg.channel.send('New prefix set.');
					}
				);
			}
		}
	},
	'say': {
		details: 'Causes the bot to say what you want. If `tts` is provided, it\'ll use the text-to-speech feature',
		run: data => {
			let voice = false;

			if (data.args[0] == 'tts') {
				data.args.shift();
				voice = true;
			}

			data.msg.delete().catch(console.error);
			data.msg.channel.send(data.args.join(' '), {tts: true});
		}
	}
};

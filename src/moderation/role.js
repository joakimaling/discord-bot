module.exports = {
	'addrole': {
		details: '?',
			run: data => {
				data.msg.guild.createRole({
					name: data.args.join(' '),
					color: 'BLUE',
				})
				.then(role => console.log(`Created new role with name ${role.name} and colour ${role.color}`))
				.catch(console.error)
			}
	}
};

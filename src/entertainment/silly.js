const randomInt = require('random-int');
const {meow} = require('../../lib/utils.js');

module.exports = {
	'accuse': {
		details: 'Sends an accusation from you to the mentioned user via DM',
		run: data => {
			meow(data.args, data.msg, 'accuse', true);
		}
	},
	'compliment': {
		details: 'Sends a compliment from you to the mentioned user via DM',
		run: data => {
			meow(data.args, data.msg, 'compliment', true);
		}
	},
	'contact': {
		details: 'Sends a message from you to the mentioned user via DM',
		run: data => {
			meow(data.args, data.msg, 'contact', true);
		}
	},
//	'divorce': {},
	'hug': {
		details: 'Sends a hug from you to the mentioned user via DM',
		run: data => {
			meow(data.args, data.msg, 'hug', false);
		}
	},
	'insult': {
		details: 'Sends an insult from you to the mentioned user via DM',
		run: data => {
			meow(data.args, data.msg, 'insult', true);
		}
	},
//	'kinky': {},
	'kiss': {
		details: 'Sends a kiss from you to the mentioned user via DM',
		run: data => {
			meow(data.args, data.msg, 'kiss', false);
		}
	},
//	'love': {},
//	'marry': {},
	'number': {
		details: 'Replaces a number sequence with clocks',
		run: data => {
			data.msg.channel.send(data.args.join().split('').map(number => {
				return number == 0 ? `:clock12:` : `:clock${number}:`
			}).join(''));
		}
	}
//	'poll': {},
//	'purge': {},
};
